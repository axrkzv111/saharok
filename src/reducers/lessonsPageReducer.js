import * as types from 'constants/actionTypes'
import mockData from '../components/pages/lessons/mockData'

const initialState = {
  lessonsData: mockData
}

export default function lessonsScreenReducer(state = initialState, action) {
  let newState = state

  switch (action.type) {
    case types.EXAMPLE_ACTION: {
      newState = {
        data: action.payload
      }
      break
    }
    default:
      break
  }

  return newState
}
