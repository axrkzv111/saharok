import { combineReducers } from 'redux'

import exampleReducer from './exampleReducer'
import lessonsPageReducer from './lessonsPageReducer'

const rootReducer = combineReducers({
  exampleReducer,
  lessonsPageReducer
})

export default rootReducer
