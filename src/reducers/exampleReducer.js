import * as types from 'constants/actionTypes'

const initialState = {}

export default function example(state = initialState, action) {
  let newState = state

  switch (action.type) {
    case types.EXAMPLE_ACTION: {
      newState = {
        data: action.payload
      }
      break
    }
    default:
      break
  }

  return newState
}
