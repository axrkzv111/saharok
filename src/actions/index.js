import * as types from 'constants/actionTypes'

export function example() {
  return {
    type: types.EXAMPLE_ACTION,
    payload: { someField: 'something' }
  }
}
