import React from 'react'
import NavigationButton from '../../shared/NavigationButton'

import './homePage.scss'

const HomePage = () => (
  <main className="home-page" role="main">
    <article className="home-page__content">
      <h1 className="home-page__heading">SaxarOK School</h1>
      <p className="home-page__text">
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
        invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
      </p>
      <p className="home-page__text">
        At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
        takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
        sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
        erat, sed diam voluptua.
      </p>
      <div className="home-page__link-group">
        <NavigationButton type="primary" to="/lessons">
          Уроки
        </NavigationButton>
        <NavigationButton type="secondary" to="/registration">
          Регистрация
        </NavigationButton>
      </div>
    </article>
  </main>
)

export default HomePage
