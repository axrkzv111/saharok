const mockData = {
  name: 'Диабет 1 типа',
  minDescription: 'Что это?',
  description: 'Ты можешь посмотреть видео, или зарегистрироваться и ввести статистику',
  id: 123,
  lessons: [
    {
      title: 'Урок 1',
      id: 1
    },
    {
      title: 'Урок 2',
      id: 2
    },
    {
      title: 'Урок 3',
      id: 3
    }
  ]
}

export default mockData
