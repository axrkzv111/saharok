import React from 'react'
import PropTypes from 'prop-types'
import SectionIcon from 'icons/lesson1'
import './section.scss'

const Section = ({ heading, minDescription, description }) => (
  <div className="lessons-page__section">
    <SectionIcon className="section__svg" />
    <div className="section__info">
      <h2 className="section__heading">{heading}</h2>
      <p className="section__min-description">({minDescription})</p>
      <p className="section__description">{description}</p>
    </div>
  </div>
)


Section.propTypes = {
  heading: PropTypes.string,
  minDescription: PropTypes.string,
  description: PropTypes.string
}

export default Section
