import React from 'react'
import PropTypes from 'prop-types'
import './lesson.scss'


const Lesson = ({ title }) => (
  <div className="lessons-page__lesson">
    <h2 className="lesson__heading">{title}</h2>
  </div>
)

Lesson.propTypes = {
  title: PropTypes.string.isRequired
}

export default Lesson
