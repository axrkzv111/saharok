import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Burger from 'icons/burger'
import Section from './Section'
import Lesson from './Lesson'
import './lessonsPage.scss'

class LessonsPage extends Component {
  render() {
    const { name, description, minDescription, lessons } = this.props.lessonsData
    return (
      <main className="lessons-page vector">
        <article className="lessons-page__content">
          <Burger className="lessons-page__burger" />
          <h1 className="lessons-page__heading">Уроки</h1>
          <p>Выбирай урок и начинай</p>
          <Section
            heading={name}
            minDescription={minDescription}
            description={description}
          />
          {lessons.map(lesson => (
            <Lesson title={lesson.title} key={lesson.id} />
          ))}
        </article>
      </main>
    )
  }
}

const mapStateToProps = store => ({
  lessonsData: store.lessonsPageReducer.lessonsData
})

LessonsPage.propTypes = {
  lessonsData: PropTypes.object.isRequired
}

export default connect(mapStateToProps)(LessonsPage)
