import React, { Component } from 'react'
import { hot } from 'react-hot-loader'
import { Switch, Route } from 'react-router-dom'
import HomePage from './pages/home'
import LessonsPage from './pages/lessons'
import DashboardPage from './pages/dashboard'

class App extends Component {
  render() {
    return (
      <div className="app">
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/lessons" component={LessonsPage} />
          <Route path="/registration" component={LessonsPage} />
          <Route path="/dashboard" component={DashboardPage} />
        </Switch>
      </div>
    )
  }
}

App.propTypes = {}

export default hot(module)(App)
