import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import centered from '@storybook/addon-centered'
import { withKnobs, selectV2, text } from '@storybook/addon-knobs'

import Button from '.'

storiesOf('Shared components', module)
  .addDecorator(centered)
  .addDecorator(withKnobs)
  .add('Button', () => (
    <Button
      type={selectV2('Type', { primary: 'primary', secondary: 'secondary' })}
      onClick={action('clicked')}
    >
      {text('Text', 'Hello Button')}
    </Button>
  ))
