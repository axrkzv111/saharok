import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './button.scss'

const Button = ({
  type,
  children,
  onClick,
  staticContext,
  match,
  location,
  history,
  ...rest
}) => (
  <button onClick={onClick} className={classnames('button', `button--${type}`)} {...rest}>
    {children}
  </button>
)

Button.defaultProps = {
  type: 'primary'
}

Button.propTypes = {
  type: PropTypes.oneOf(['primary', 'secondary']),
  children: PropTypes.node,
  onClick: PropTypes.func
}

export default Button
