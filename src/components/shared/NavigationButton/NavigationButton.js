import { withRouter } from 'react-router-dom'
import { compose, withHandlers } from 'recompose'
import Button from '../Button'

const NavigationButton = compose(
  withRouter,
  withHandlers({
    onClick: ({ history, to }) => () => history.push(to)
  })
)(Button)

export default NavigationButton
