import React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs, number } from '@storybook/addon-knobs'
import pascalCase from 'pascal-case'

const req = require.context('icons', true, /.svg$/)

const stories = storiesOf('Icon', module).addDecorator(withKnobs)

req.keys().forEach((filename) => {
  const Icon = req(filename).default
  const prefixLength = './'.length
  const endingLength = '.svg'.length
  const stroryName = pascalCase(filename.slice(prefixLength, -endingLength))
  stories.add(stroryName, () => (
    <Icon width={number('Width', 500)} height={number('Height', 500)} />
  ))
})
