import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

export default class Icon extends PureComponent {
  render() {
    const { className, glyph, ...restProps } = this.props

    return (
      <svg className={className} {...restProps}>
        <use xlinkHref={`#${glyph}`} />
      </svg>
    )
  }
}

Icon.propTypes = {
  glyph: PropTypes.string.isRequired,
  className: PropTypes.string
}

Icon.defaultProps = {
  className: ''
}
