import React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs, selectV2, text, boolean, array } from '@storybook/addon-knobs'
import centered from '@storybook/addon-centered'
import { action } from '@storybook/addon-actions'

import Dropdown from '.'
import { StatefullDropdown } from './Dropdown'

storiesOf('Shared components', module)
  .addDecorator(centered)
  .addDecorator(withKnobs)
  .add('Dropdown', () => (
    <Dropdown
      onClick={action('clicked')}
      onSelect={action('selected')}
      isOpen={boolean('isOpen', false)}
      title={text('Title', 'dropdown')}
      list={array('Select Oprions', ['Option1', 'Option2']).map((title, id) => ({
        id,
        title
      }))}
      direction={selectV2('Direction', { up: 'up', down: 'down' }, 'up')}
    />
  ))
  .add('Stateful Dropdown', () => (
    <StatefullDropdown
      onSelect={action('selected')}
      title={text('Title', 'dropdown')}
      list={array('Select Oprions', ['Option1', 'Option2']).map((title, id) => ({
        id,
        title
      }))}
      direction={selectV2('Direction', { up: 'up', down: 'down' }, 'up')}
    />
  ))
