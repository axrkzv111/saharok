import React from 'react'
import PropTypes from 'prop-types'
import onClickOutside from 'react-onclickoutside'
import classNames from 'classnames'
import { withState, withHandlers, compose } from 'recompose'
import './dropdown.scss'

const Dropdown = ({ isOpen, onClick, onSelect, list, direction, title }) => (
  <div className={classNames('dropdown', `dropdown--${direction}`)}>
    <header className="dropdown__header" onClick={onClick}>
      <p className="dropdown__header-title">{title}</p>
    </header>
    {isOpen && (
      <div className="dropdown__list">
        {list.map(item => (
          <div onClick={onSelect} className="dropdown__list-item" key={item.id}>
            {item.title}
          </div>
        ))}
      </div>
    )}
  </div>
)

Dropdown.propTypes = {
  isOpen: PropTypes.bool,
  onSelect: PropTypes.func,
  onClick: PropTypes.func,
  title: PropTypes.string,
  list: PropTypes.array.isRequired,
  direction: PropTypes.oneOf(['up', 'down'])
}

Dropdown.defaultProps = {
  direction: 'down'
}

export default Dropdown

export const StatefullDropdown = compose(
  withState('isOpen', 'setOpen'),
  withHandlers({
    handleClickOutside: ({ setOpen }) => () => setOpen(false),
    onClick: ({ setOpen }) => () => setOpen(isOpen => !isOpen)
  }),
  onClickOutside
)(Dropdown)
