import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { withKnobs, selectV2, text } from '@storybook/addon-knobs'

import Input from '.'

const req = require.context('icons', true, /.svg$/)

storiesOf('Shared components', module)
  .addDecorator(withKnobs)
  .add('Input', () => {
    const iconName = selectV2('Icon', req.keys(), './reg-input-name.svg')
    const Icon = req(iconName).default
    return <Input Icon={Icon} value={text('Value')} onChange={action('changerd')} />
  })
