import React from 'react'
import PropTypes from 'prop-types'
import './input.scss'

const Input = ({ Icon, ...rest }) => (
  <div className="input">
    { Icon && <Icon className="input__icon" />}
    <input type="text" className="input__text" {...rest} />
  </div>
)

Input.defaultProps = {
  Icon: undefined
}

Input.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  Icon: PropTypes.node
}

export default Input
