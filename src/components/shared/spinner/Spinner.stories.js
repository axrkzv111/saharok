import React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs'

import Spinner from '.'

storiesOf('Shared components', module)
  .addDecorator(withKnobs)
  .add('Spinner', () => (
    <Spinner
      color={text('Color', '#0062cc')}
      size={number('Size', 15)}
      loading={boolean('Loading', true)}
    />
  ))
