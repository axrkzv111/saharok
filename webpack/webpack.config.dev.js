const { resolve } = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
// const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const srcFolder = f => resolve(__dirname, '../src', `${f}/`)

module.exports = {
  mode: 'development',
  resolve: {
    extensions: ['.js', '.jsx', '.svg'],
    alias: {
      icons: resolve(__dirname, '../assets/icons'),
      components: srcFolder('components'),
      constants: srcFolder('constants'),
      actions: srcFolder('actions'),
      configs: resolve(__dirname, '../configs/'),
      scss: srcFolder('scss')
    }
  },
  entry: [
    'babel-polyfill',
    'webpack-dev-server/client',
    'webpack/hot/only-dev-server',
    resolve(__dirname, '../src'),
    resolve(__dirname, '../src/scss/index.scss')
  ],
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname),
    publicPath: '/'
  },
  context: resolve(__dirname, '../src'),
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    host: 'localhost',
    contentBase: resolve(__dirname, '../assets'),
    publicPath: '/',
    historyApiFallback: true
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: [resolve(__dirname, '../src'), resolve(__dirname)],
        use: 'babel-loader'
      },
      {
        test: /\.scss/,
        use: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'file-loader?name=public/fonts/[name].[ext]'
      },
      {
        test: /\.svg$/,
        include: [resolve(__dirname, '../assets/icons')],
        use: [
          {
            loader: 'babel-loader'
          },
          {
            loader: 'svg-sprite-loader',
            options: {
              runtimeGenerator: require.resolve('./tools/svg-to-icon-component-runtime-generator'),
              runtimeOptions: {
                iconModule: 'components/shared/Icon'
              }
            }
          }
        ]
      },
      {
        test: /\.(svg|png)$/,
        include: [resolve(__dirname, '../assets/background-img')],
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            }
          }
        ]
      }


    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),

    new HtmlWebpackPlugin({
      title: 'saharok',
      template: resolve(__dirname, './template.html')
    })
    // new BundleAnalyzerPlugin()
    // new MiniCssExtractPlugin({
    //   filename: 'bundle.css'
    // })
  ],
  performance: { hints: false }
}
