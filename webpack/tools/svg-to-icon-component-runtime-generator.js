const pascalCase = require('pascal-case')
const { stringifyRequest } = require('loader-utils')
const { stringifySymbol } = require('svg-sprite-loader/lib/utils')

module.exports = function runtimeGenerator({ symbol, config, context }) {
  const { spriteModule, symbolModule, runtimeOptions } = config

  const spriteRequest = stringifyRequest({ context }, spriteModule)
  const symbolRequest = stringifyRequest({ context }, symbolModule)
  const parentComponentDisplayName = 'SpriteSymbolComponent'
  const displayName = `${pascalCase(symbol.id)}${parentComponentDisplayName}`

  return `
    import React from 'react';
    import SpriteSymbol from ${symbolRequest};
    import sprite from ${spriteRequest};
    import ${parentComponentDisplayName} from '${runtimeOptions.iconModule}';
    
    const symbol = new SpriteSymbol(${stringifySymbol(symbol)});
    sprite.add(symbol);
    export default class ${displayName} extends React.Component {
      render() {
        return <${parentComponentDisplayName} glyph="${symbol.id}" {...this.props} />;
      }
    }
  `
}
